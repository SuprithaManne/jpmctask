//
//  ViewController.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import UIKit
import MapKit
import SDWebImage
import Reachability

class HomeLanding: UIViewController, UITableViewDataSource, UITableViewDelegate, CityViewModelProtocol, WeatherInfoViewModelProtocol {

   
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_CityName: UILabel!
    @IBOutlet weak var imgV_Weather: UIImageView!
    @IBOutlet weak var lbl_Weather: UILabel!
    
    @IBOutlet weak var lbl_Temperature: UILabel!
    @IBOutlet weak var lbl_Humidity: UILabel!
    @IBOutlet weak var lbl_Wind: UILabel!
    @IBOutlet weak var lbl_MinAndMaxTemp: UILabel!
    @IBOutlet weak var view_WeatherCondition: UIView!
    
    @IBOutlet weak var view_Search: UIView!
    @IBOutlet weak var tv_City: UITableView!
    @IBOutlet weak var btn_Search: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let kDefaultCellIdentifier = "MapSearchViewControllerTableViewCell"
    var viewModel = CityDataViewModel(apiService: APIManager.init())
    var weatherViewModel = WeatherInfoDataViewModel(apiService: APIManager.init())
    var data : [CityData]?
    var weatherInfoData : WeatherInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tv_City.isHidden = true
        self.tv_City.dataSource = self
        self.tv_City.delegate = self
        self.searchBar.delegate = self
        self.viewModel.viewModelDelegate = self
        self.weatherViewModel.viewModelDelegate = self
        self.view_WeatherCondition.layer.cornerRadius = 16.0
        self.view_WeatherCondition.layer.borderWidth = 1
        self.view_WeatherCondition.layer.borderColor = .init(red: 63.0/255.0, green: 112.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        self.view_WeatherCondition.backgroundColor = .init(red: 63.0/255.0, green: 112.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        self.view_WeatherCondition.layer.shadowColor = UIColor.white.cgColor
        self.lbl_DateTime.text = convertDateAndTime()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Get Lat and Long Values based on Location
        LocationManager.shared.getLocation { (location:CLLocation?, error:NSError?) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let location = location else {
                return
            }
            print("Latitude: \(location.coordinate.latitude) Longitude: \(location.coordinate.longitude)")
                        
            self.weatherViewModel.getWeatherInfo(url: "http://api.openweathermap.org/data/2.5/weather?lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&appid=24e1bcd1a967fb162931527d46c953dd")
        }
        
        LocationManager.shared.getCurrentReverseGeoCodedLocation { (location:CLLocation?, placemark:CLPlacemark?, error:NSError?) in
                    
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                    
                    guard let location = location, let placemark = placemark else {
                        return
                    }
                    
                    //We get the complete placemark and can fetch anything from CLPlacemark
//            print(placemark.country ?? "", placemark.isoCountryCode)
            self.lbl_CityName.text = "\(placemark.addressDictionary?["Country"] ?? ""),\(placemark.addressDictionary?["CountryCode"] ?? "")"
        }
    }
    
    // Mark:- City Service Respnse Success
    func updateSuccessResponse(cityResponse: [CityData]) {
        DispatchQueue.main.async { [self] in
            self.data = cityResponse
            self.tv_City.reloadData()
            self.tv_City.isHidden = false
        }
    }
    
    // Mark:- City Service Respnse Failure
    func updateFailureResponse(error: Error) {
        DispatchQueue.main.async {
            self.displayAlert(titleStr: "Warning", messageStr: "Service Unavailable.")
        }
    }
    
    // Mark:- Weather Service Respnse Success
    func updateWeatherSuccessResponse(weatherResponse: WeatherInfo) {
        DispatchQueue.main.async {
            self.weatherInfoData = weatherResponse
            if let info = self.weatherInfoData, let mainInfo = info.main, let weather = info.weather {
                self.lbl_Humidity.text = "Humidity : \(mainInfo.humidity ?? 0)"
                self.lbl_Wind.text = "WindSpeed : \(info.wind?.speed ?? 0.0)"
                let minTemp = "Min Temp : \(mainInfo.tempMin ?? 0.0)"
                let maxTemp = "Max Temp : \(mainInfo.tempMax ?? 0.0)"
                self.lbl_MinAndMaxTemp.text = "\(minTemp)\n\(maxTemp)"
                let tempValue = self.convertTemp(temp: mainInfo.temp ?? 0.0, from: .kelvin, to: .celsius)
                let dateTime = self.convertDateAndTime(dateTime: info.dt ?? 0)
                self.lbl_Temperature.text = tempValue
                self.lbl_DateTime.text = dateTime
                if weather.count > 0 {
                    self.lbl_Weather.text = weather[0].main
                    self.imgV_Weather.image = UIImage(named: "pencil.circle.fill")
//                    self.imgV_Weather.sd_setImage(with: URL(string: "https://openweathermap.org/img/wn/\(weather[0].icon ?? "").png"))
                }
                self.lbl_CityName.text = "\(info.name ?? ""), \(info.sys?.country ?? "")"

            }
        }
    }
    
    // Mark:- Weather Service Respnse Failure
    func updateWeatherFailureResponse(error: Error) {
        DispatchQueue.main.async {
            self.displayAlert(titleStr: "Warning", messageStr: "Service Unavailable.")
        }
    }
    
    // Mark:- TableView DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityInfoTableViewCell", for: indexPath) as! CityInfoTableViewCell
        if let info = data?[indexPath.row] {
            cell.lbl_CityName.text =  "\(info.state),\(info.country)"
            cell.lbl_LatLong.text = String(format: "%f,%f",info.lat, info.lon)
        }
        return cell
    }
    
    // Mark:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tv_City.isHidden = true
        if let indexValue = data?[indexPath.row] {
            weatherViewModel.getWeatherInfo(url: "http://api.openweathermap.org/data/2.5/weather?lat=\(indexValue.lat)&lon=\(indexValue.lon)&appid=24e1bcd1a967fb162931527d46c953dd")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    // Mark:- Search Button Tapped
    @IBAction func btn_SwitchAction(_ sender: UIButton) {
        guard reachabilityChanged() else {
            self.displayAlert(titleStr: "No internet connection", messageStr: "Please try again when you have a better connection.")
            return
        }
        if let searchText = searchBar.text {
            viewModel.fetchCityInfo(url: "http://api.openweathermap.org/geo/1.0/direct?q=\(searchText.lowercased())&limit=5&appid=24e1bcd1a967fb162931527d46c953dd")
        }
    }
    
    // Mark:- Alert
    func displayAlert(titleStr: String, messageStr : String) {
        let dialogMessage = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert)
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
        })
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    // Mark:- Temperature Conversion
    func convertTemp(temp: Double, from inputTempType: UnitTemperature, to outputTempType: UnitTemperature) -> String {
        let mf = MeasurementFormatter()
        mf.numberFormatter.maximumFractionDigits = 0
        mf.unitOptions = .providedUnit
        let input = Measurement(value: temp, unit: inputTempType)
        let output = input.converted(to: outputTempType)
        return mf.string(from: output)
      }
    
    // Mark:- Formatting Date&Time
    func convertDateAndTime(dateTime : Int) -> String? {
        let date = Date(timeIntervalSince1970: TimeInterval((dateTime)))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE MMM dd yyyy hh:mm"
        return dateFormatter.string(from: date)
    }
    // Mark:- Formatting Current Date
    func convertDateAndTime() -> String? {
        let currentDate = Date()
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEE MMM dd, yyyy hh:mm"
        return dateFormatter1.string(from: currentDate)
    }
        
}

// Mark:- SearchBar Delegate
extension HomeLanding : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

