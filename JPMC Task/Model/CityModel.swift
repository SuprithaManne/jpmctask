//
//  CityModel.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import Foundation
import UIKit

struct CityData: Codable {
    let name: String
//    let localNames: LocalNames?
    let lat, lon: Double
    let country, state: String

    enum CodingKeys: String, CodingKey {
        case name
//        case localNames = "local_names"
        case lat, lon, country, state
    }
}

// MARK: - LocalNames
//struct LocalNames: Codable {
//    let en, lt: String
//    let ta: String?
//}
