//
//  Reachable.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import Foundation
import Reachability


var isNetworkReachable = false

func reachabilityChanged() -> Bool {

    let reachability = try! Reachability()

    switch reachability.connection {
        case .wifi:
            isNetworkReachable = true
        case .cellular:
            isNetworkReachable = true
        case .unavailable:
            isNetworkReachable = false
        default :
            isNetworkReachable = false
    }
    return isNetworkReachable
}
