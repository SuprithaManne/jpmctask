//
//  CityViewModel.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import Foundation

protocol CityViewModelProtocol: AnyObject {
    func updateSuccessResponse(cityResponse : [CityData])
    func updateFailureResponse(error : Error)
}

class CityDataViewModel {
    /// Variable of type APIManagerProtocol to get The Data from Service
    var apiService : APIManagerProtocol
    weak var viewModelDelegate : CityViewModelProtocol?
    
    init(apiService : APIManagerProtocol) {
        self.apiService = apiService
    }
    
    /// Get City Information from Service
    /// - Parameter url: String
    func fetchCityInfo(url:String){
        self.apiService.fetch(url: url, model: [CityData].self) { result in
            switch result {
            case .success(let response) :
                if let cityResp = response as? [CityData] {
                    self.viewModelDelegate?.updateSuccessResponse(cityResponse: cityResp)
                }
            case .failure(let error):
                self.viewModelDelegate?.updateFailureResponse(error: error)
            }
        }
    }
    
}
