//
//  CItyInfoViewModel.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import Foundation

protocol WeatherInfoViewModelProtocol: AnyObject {
    func updateWeatherSuccessResponse(weatherResponse : WeatherInfo)
    func updateWeatherFailureResponse(error : Error)
}

class WeatherInfoDataViewModel {
    /// Variable of type APIManagerProtocol to get The Data from Service
    var apiService : APIManagerProtocol
    weak var viewModelDelegate : WeatherInfoViewModelProtocol?
    
    init(apiService : APIManagerProtocol) {
        self.apiService = apiService
    }
    
    /// Get Weather Information from Service
    /// - Parameter url: String
    func getWeatherInfo(url:String){
        self.apiService.fetch(url: url, model: WeatherInfo.self) { result in
            switch result {
            case .success(let response) :
                if let weatherResp = response as? WeatherInfo {
                    self.viewModelDelegate?.updateWeatherSuccessResponse(weatherResponse: weatherResp)
                }
            case .failure(let error):
                self.viewModelDelegate?.updateWeatherFailureResponse(error: error)
            }
        }
    }
    
}
