//
//  CityInfoTableViewCell.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import UIKit

class CityInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_CityName: UILabel!
    @IBOutlet weak var lbl_LatLong: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
