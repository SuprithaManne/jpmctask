//
//  APIManager.swift
//  JPMC Task
//
//  Created by Supritha manne on 07/03/23.
//

import Foundation
import UIKit

enum APIError : Error {
    case invalidUrl
    case responseFailed
}

/// Protocol to fetch the data from Service
protocol APIManagerProtocol {
    func fetch<T:Codable>(url: String, model: T.Type, completionhandler : @escaping (Result<Any, Error>) -> Void)
        
    }

class APIManager : APIManagerProtocol {
    func fetch<T:Codable>(url: String, model: T.Type, completionhandler : @escaping (Result<Any, Error>) -> Void) {
        guard let urlString = URL(string: url) else {
            completionhandler(.failure(APIError.invalidUrl))
            return
        }
        var urlRequest = URLRequest(url: urlString)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: urlRequest) { data, error, response in
            guard let validData = data else {
                completionhandler(.failure(APIError.responseFailed))
                return
            }
            do {
                let jsonResp = try JSONDecoder().decode(T.self, from: validData)
                completionhandler(.success(jsonResp))
            }catch let error{
                completionhandler(.failure(error))
            }
        }.resume()
    }
}
